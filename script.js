angular.
    module("counter", []). 
    controller("UpdateCounter", function(){
    this.number = 0; 

    this.increment = () => this.number++; 

    this.decrement = () => this.number--; 
})